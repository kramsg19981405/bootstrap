#!/bin/bash

STORAGE_PATH="/srv/http/public/manjaro"

rsync \
  --recursive \
  --links \
  --perms \
  --times \
  --compress \
  --progress \
  --delete \
  rsync://mirrorservice.org/repo.manjaro.org/repos \
  $STORAGE_PATH
